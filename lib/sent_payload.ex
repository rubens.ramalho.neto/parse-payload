defmodule SentPayload do
  @domain "https://delivery-center-recruitment-ap.herokuapp.com"

  def validate_payload(file) do
    with {:ok, payload} <- ParsePayload.retrive_payload(file) do
      payload
      |> Jason.encode!()
      |> request()
      |> handle_response()
    end
  end

  defp request(payload) do
    HTTPoison.request(:post, @domain, payload, header())
  end

  defp header() do
    [{"X-Sent", formated_date_time()}]
  end

  defp formated_date_time() do
    date = DateTime.utc_now()
    "#{date.hour}h#{date.minute} - #{date.day}/#{date.month}/#{date.year}"
  end

  defp handle_response({:ok, %HTTPoison.Response{body: "OK"}}), do: {:ok, "Successful"}
  defp handle_response({:ok, %HTTPoison.Response{body: body}}), do: {:error, body}
  defp handle_response({:error, _reason} = error), do: error
end
