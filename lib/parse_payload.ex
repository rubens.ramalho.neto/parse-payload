defmodule ParsePayload do
  def retrive_payload(file) do
    file
    |> get_payload()
    |> prepare_payload()
  end

  defp get_payload(filename) do
    with {:ok, body} <- File.read(filename),
         {:ok, json} <- Jason.decode(body, keys: :atoms),
         do: {:ok, json}
  end

  defp prepare_payload(
         {:ok,
          %{
            shipping: %{receiver_address: receiver_address},
            buyer: customer,
            payments: payments,
            order_items: order_items
          } = payload}
       ) do
    {:ok, dtOrder, _} = DateTime.from_iso8601(payload.date_created)
    dtOrderIso8601 = DateTime.to_iso8601(dtOrder)

    {:ok,
     %{
       externalCode: "#{payload.id}",
       storeId: payload.store_id,
       subTotal: "#{payload.total_amount}",
       deliveryFee: "#{payload.total_shipping}",
       total_shipping: "#{payload.total_shipping}",
       total: "#{payload.total_amount_with_shipping}",
       dtOrderCreate: dtOrderIso8601,
       country: receiver_address.country.id,
       state: "SP",
       city: receiver_address.city.name,
       district: receiver_address.neighborhood.name,
       street: receiver_address.street_name,
       complement: receiver_address.comment,
       latitude: receiver_address.latitude,
       longitude: receiver_address.longitude,
       postalCode: receiver_address.zip_code,
       number: receiver_address.street_number,
       customer: prepare_custumer_payload(customer),
       items: handle_order_items(order_items),
       payments: handle_payments(payments)
     }}
  end

  defp prepare_payload({:error, _reason} = error), do: error
  defp prepare_payload({:ok, _payload}), do: {:error, "Missing params!"}

  defp prepare_custumer_payload(%{id: id, nickname: nickname, email: email, phone: phone}) do
    %{
      externalCode: "#{id}",
      name: nickname,
      email: email,
      contact: "#{phone.area_code}#{phone.number}"
    }
  end

  defp prepare_custumer_payload(_), do: %{}

  defp handle_order_items(items) when length(items) != 0 do
    Enum.map(items, &prepare_item_payload/1)
  end

  defp handle_order_items([]), do: []

  defp prepare_item_payload(%{
         item: %{id: id, title: title},
         quantity: quantity,
         unit_price: unit_price,
         full_unit_price: full_unit_price
       }) do
    %{
      externalCode: id,
      name: title,
      price: unit_price,
      quantity: quantity,
      total: full_unit_price,
      subItems: []
    }
  end

  defp prepare_item_payload(_), do: %{}

  defp handle_payments(payments) when length(payments) != 0 do
    Enum.map(payments, &prepare_payment_payload/1)
  end

  defp handle_payments([]), do: []

  defp prepare_payment_payload(%{
         payment_type: payment_type,
         total_paid_amount: total_paid_amount
       }) do
    %{
      type: String.upcase(payment_type),
      value: total_paid_amount
    }
  end

  defp prepare_payment_payload(_), do: %{}
end
