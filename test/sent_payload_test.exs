defmodule SentPayloadTest do
  use ExUnit.Case

  describe "validate_payload/1" do
    test "returns ok when the payload id valid" do
      assert {:ok, "Successful"} = SentPayload.validate_payload("payload.json")
    end

    test "returns error when returned" do
      assert {:error, _} = SentPayload.validate_payload("wrong.json")
    end
  end
end
