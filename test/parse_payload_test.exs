defmodule ParsePayloadTest do
  use ExUnit.Case

  describe "retrive_payload/1" do
    test "returns parsed struct when exist a valid payload" do
      assert {:ok,
              %{
                externalCode: externalCode,
                dtOrderCreate: dtOrderCreate,
                items: items,
                customer: customer,
                payments: payments
              }} = ParsePayload.retrive_payload("payload.json")
    end

    test "returns error when received a invalid payload" do
      assert {:error, _} = ParsePayload.retrive_payload("not_exist.json")
    end
  end
end
