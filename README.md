# ParsePayload

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `parse_payload` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:parse_payload, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/parse_payload](https://hexdocs.pm/parse_payload).

